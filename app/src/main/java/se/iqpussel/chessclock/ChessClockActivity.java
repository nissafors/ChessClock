package se.iqpussel.chessclock;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

/**
 * Activity for showing and running a chess clock
 *
 * @author Andreas Andersson
 */
public class ChessClockActivity extends AppCompatActivity {
    private static String TAG = "ChessClockActivity";

    // Enums

    private enum MultiButtonState { SETTINGS, PAUSE, RESET }

    // Constants

    /**
     * Used to communicate with the {@link SettingsActivity}
     */
    public static final int SETTINGS_REQUEST_CODE = 1;

    private static final String RESTORE_WHITE_TIME = "restoreWhiteTime";
    private static final String RESTORE_BLACK_TIME = "restoreBlackTime";
    private static final String RESTORE_WHITE_TIME_LEFT = "restoreWhiteTimeLeft";
    private static final String RESTORE_BLACK_TIME_LEFT = "restoreBlackTimeLeft";
    private static final String RESTORE_FULLSCREEN = "restoreFullscreen";

    // Views

    private View fullscreenView;
    private ImageButton multiButton;
    private ImageButton whiteButton;
    private TextView whiteButtonText;
    private ImageButton blackButton;
    private TextView blackButtonText;

    // Services

    private ChessClockService chessClockService;

    // Settings

    private long resetWhiteTo;
    private long resetBlackTo;
    private long restoreWhiteTimeLeftTo;
    private long restoreBlackTimeLeftTo;
    private boolean screenKeepOnIsSet = false;
    private boolean fullscreenMode = false;
    private MultiButtonState currentMultiButtonState = MultiButtonState.SETTINGS;

    // Flags
    private boolean bound = false;
    private boolean newSetting = false;


    // AppCompatActivity

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Restore state
        restoreSettingsFromStorage();
        restoreWhiteTimeLeftTo = resetWhiteTo;
        restoreBlackTimeLeftTo = resetBlackTo;

        if (savedInstanceState != null) {
            resetWhiteTo = savedInstanceState.getLong(RESTORE_WHITE_TIME, resetWhiteTo);
            resetBlackTo = savedInstanceState.getLong(RESTORE_BLACK_TIME, resetBlackTo);
            restoreWhiteTimeLeftTo = savedInstanceState.getLong(RESTORE_WHITE_TIME_LEFT, restoreWhiteTimeLeftTo);
            restoreBlackTimeLeftTo = savedInstanceState.getLong(RESTORE_BLACK_TIME_LEFT, restoreBlackTimeLeftTo);
            fullscreenMode = savedInstanceState.getBoolean(RESTORE_FULLSCREEN, false);
        }

        // Set layout
        setContentView(R.layout.activity_chess_clock);
        getSupportActionBar().hide();

        // Find views
        fullscreenView = findViewById(R.id.main_relative_layout);
        multiButton = (ImageButton) findViewById(R.id.multi_button);
        whiteButton = (ImageButton) findViewById(R.id.white_switch_image_button);
        whiteButtonText = (TextView) findViewById(R.id.white_switch_text_view);
        blackButton = (ImageButton) findViewById(R.id.black_switch_image_button);
        blackButtonText = (TextView) findViewById(R.id.black_switch_text_view);

        // Set listeners
        whiteButton.setOnTouchListener(circleTouchListener);
        blackButton.setOnTouchListener(circleTouchListener);
        multiButton.setOnClickListener(multiButtonListener);
        whiteButton.setOnClickListener(whiteButtonListener);
        blackButton.setOnClickListener(blackButtonListener);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            // Now that we can read dimensions, adjust layout:
            adjustLayout();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        // Bind to ChessClockService - The heart of the application
        Intent intent = new Intent(this, ChessClockService.class);
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Results from SettingsActivity
        if (resultCode == SETTINGS_REQUEST_CODE) {

            // Get and update data
            resetWhiteTo = data.getLongExtra(SettingsActivity.WHITE_MILLIS, 0);
            resetBlackTo = data.getLongExtra(SettingsActivity.BLACK_MILLIS, 0);
            boolean newFullscreenSetting = data.getBooleanExtra(SettingsActivity.FULLSCREEN_MODE, false);
            restoreWhiteTimeLeftTo = resetWhiteTo;
            restoreBlackTimeLeftTo = resetBlackTo;
            newSetting = true;

            if (newFullscreenSetting != fullscreenMode) {
                fullscreenMode = newFullscreenSetting;
                setFullscreenFlags();
                adjustLayout();
            }

            writeSettingsToStorage();

            if (bound) {
                Log.d(TAG, "onActivityResult: Lo and behold! We were already bound!");
                // Will probably not fire, but in case this is slower than bind to service:
                chessClockService.setState(ChessClockService.State.PAUSED);
                chessClockService.setClocks(resetWhiteTo, resetBlackTo);
                setButtons();
                newSetting = false;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (bound) {
            // Restore after coming back from paused state.
            chessClockService.toBackground();
            setButtons();
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();

        if (fullscreenMode)
            setFullscreenFlags();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // Only need save service state when not counting, as the service will then go foreground instead.
        if (bound && (chessClockService.getState() == ChessClockService.State.PAUSED || chessClockService.getState() == ChessClockService.State.OUT_OF_TIME)) {
            outState.putLong(RESTORE_WHITE_TIME, resetWhiteTo);
            outState.putLong(RESTORE_BLACK_TIME, resetBlackTo);
            outState.putLong(RESTORE_WHITE_TIME_LEFT, chessClockService.getWhiteMillisLeft());
            outState.putLong(RESTORE_BLACK_TIME_LEFT, chessClockService.getBlackMillisLeft());
        }

        outState.putBoolean(RESTORE_FULLSCREEN, fullscreenMode);
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (bound) {
            if (chessClockService.getState() == ChessClockService.State.WHITE_COUNTING || chessClockService.getState() == ChessClockService.State.BLACK_COUNTING) {
                chessClockService.toForeground();
            }
            else {
                // Keep up to date as these will be used to restore state if activity is not destroyed
                restoreWhiteTimeLeftTo = chessClockService.getWhiteMillisLeft();
                restoreBlackTimeLeftTo = chessClockService.getBlackMillisLeft();
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (bound) {
            unbindService(serviceConnection);
            chessClockService = null;
            bound = false;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    // ChessClockService

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            bound = true;


            // Get instance
            ChessClockService.ChessClockBinder binder = (ChessClockService.ChessClockBinder) iBinder;
            chessClockService = binder.getService();

            Log.d(TAG, "onServiceConnected: " + chessClockService.getState().toString());

            // Restore state
            if (!newSetting && chessClockService.isActive()) {
                Log.d(TAG, "onServiceConnected: New setting or from foreground");
                // We're coming back from running service in foreground.
                if (chessClockService.getState() == ChessClockService.State.OUT_OF_TIME) {
                    Log.d(TAG, "onServiceConnected: Out of time");
                    restoreWhiteTimeLeftTo = chessClockService.getWhiteMillisLeft();
                    restoreBlackTimeLeftTo = chessClockService.getBlackMillisLeft();
                }

                chessClockService.toBackground();
            }
            else {
                // New or restored instance, or settings just changed.
                Log.d(TAG, "onServiceConnected: Reset clocks");
                chessClockService.setClocks(resetWhiteTo, resetBlackTo, restoreWhiteTimeLeftTo, restoreBlackTimeLeftTo);
            }

            // Clear flags and update UI
            newSetting = false;
            setButtons();

            // Register broadcast listener
            LocalBroadcastManager.getInstance(ChessClockActivity.this).registerReceiver(tickReceiver, new IntentFilter(ChessClockService.CHESS_CLOCK_SERVICE_ID_STRING));
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            bound = false;
        }
    };


    // Listeners

    View.OnClickListener multiButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (currentMultiButtonState) {
                case SETTINGS:
                    // Start SettingsActivity
                    Intent intent = new Intent(ChessClockActivity.this, SettingsActivity.class);
                    intent.putExtra(SettingsActivity.WHITE_MILLIS, resetWhiteTo);
                    intent.putExtra(SettingsActivity.BLACK_MILLIS, resetBlackTo);
                    intent.putExtra(SettingsActivity.FULLSCREEN_MODE, fullscreenMode);
                    int requestCode = SETTINGS_REQUEST_CODE;
                    startActivityForResult(intent, requestCode);
                    break;
                case PAUSE:
                    if (bound) {
                        chessClockService.setState(ChessClockService.State.PAUSED);

                        // If the clock is paused we don't need to keep the screen on
                        if (screenKeepOnIsSet)
                            screenKeepOnIsSet = setKeepScreenOn(false);

                        setButtons();
                    }
                    break;
                case RESET:
                    if (bound) {
                        restoreSettingsFromStorage();
                        chessClockService.setClocks(resetWhiteTo, resetBlackTo);
                        setButtons();
                    }
                    break;
            }
        }
    };

    View.OnTouchListener circleTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            // Set a circular touch area. Consume events outside of circle so onClick won't trigger.
            if (view instanceof ImageButton) {
                int radius;
                double clickRadius;
                float originX, originY;
                ImageButton ib = (ImageButton) view;
                radius = Math.min(ib.getWidth(), ib.getHeight()) / 2;
                originX = ib.getX() + ib.getWidth() / 2;
                originY = ib.getY() + ib.getHeight() / 2;
                clickRadius = Math.sqrt(Math.pow(Math.abs(motionEvent.getX() - originX), 2) + Math.pow(Math.abs(motionEvent.getY() - originY), 2));
                if (clickRadius <= radius) {
                    return false;
                }
            }

            return true;
        }
    };

    View.OnClickListener whiteButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (bound) {
                chessClockService.setState(ChessClockService.State.BLACK_COUNTING);

                // Keep the screen on while counting
                if (!screenKeepOnIsSet)
                    screenKeepOnIsSet = setKeepScreenOn(true);

                setButtons();
            }
        }
    };

    View.OnClickListener blackButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (bound) {
                chessClockService.setState(ChessClockService.State.WHITE_COUNTING);

                // Keep the screen on while counting
                if (!screenKeepOnIsSet)
                    screenKeepOnIsSet = setKeepScreenOn(true);

                setButtons();
            }
        }
    };


    // Receivers

    BroadcastReceiver tickReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            boolean timeOut = updateButtonText();
            if (timeOut)
                setButtons();
        }
    };


    // Methods

    // Print time left in buttons. Return true on chess clock time out.
    private boolean updateButtonText() {
        if (bound) {
            long whiteTimeLeft = chessClockService.getWhiteMillisLeft();
            long blackTimeLeft = chessClockService.getBlackMillisLeft();

            whiteButtonText.setText(millisToString(whiteTimeLeft));
            blackButtonText.setText(millisToString(blackTimeLeft));
            if (whiteTimeLeft <= 0 || blackTimeLeft <= 0)
                return true;
        }

        return false;
    }

    // Set flags to view this activity in fullscreen mode
    private void setFullscreenFlags() {
        if (fullscreenMode) {
            fullscreenView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
        else {
            fullscreenView.setSystemUiVisibility(0);
        }
    }


    // Enable and disable buttons and set their colors based on chessClock state
    private void setButtons() {
        whiteButtonText.setText(millisToString(chessClockService.getWhiteMillisLeft()));
        blackButtonText.setText(millisToString(chessClockService.getBlackMillisLeft()));
        ChessClockService.State state = chessClockService.getState();

        switch (state) {
            case OUT_OF_TIME:
                multiButton.setImageResource(R.drawable.button_reset);
                currentMultiButtonState = MultiButtonState.RESET;
                if (chessClockService.getWhiteMillisLeft() <= 0) {
                    whiteButton.setImageResource(R.drawable.switch_timeout);
                    blackButton.setImageResource(R.drawable.switch_paused);
                }
                else {
                    whiteButton.setImageResource(R.drawable.switch_paused);
                    blackButton.setImageResource(R.drawable.switch_timeout);
                }
                break;

            case WHITE_COUNTING:
                multiButton.setImageResource(R.drawable.button_pause);
                currentMultiButtonState = MultiButtonState.PAUSE;
                whiteButton.setImageResource(R.drawable.switch_counting);
                blackButton.setImageResource(R.drawable.switch_paused);
                break;

            case BLACK_COUNTING:
                multiButton.setImageResource(R.drawable.button_pause);
                currentMultiButtonState = MultiButtonState.PAUSE;
                whiteButton.setImageResource(R.drawable.switch_paused);
                blackButton.setImageResource(R.drawable.switch_counting);
                break;

            default:
                if (chessClockService.getWhiteMillisLeft() == resetWhiteTo && chessClockService.getBlackMillisLeft() == resetBlackTo) {
                    // Clock is reset - show settings button
                    multiButton.setImageResource(R.drawable.button_settings);
                    currentMultiButtonState = MultiButtonState.SETTINGS;
                }
                else {
                    // Show reset button
                    multiButton.setImageResource(R.drawable.button_reset);
                    currentMultiButtonState = MultiButtonState.RESET;
                }

                whiteButton.setImageResource(R.drawable.switch_paused);
                blackButton.setImageResource(R.drawable.switch_paused);
        }
    }

    // Write settings to shared preferences
    private void writeSettingsToStorage() {
        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putLong(SettingsActivity.WHITE_MILLIS, resetWhiteTo);
        editor.putLong(SettingsActivity.BLACK_MILLIS, resetBlackTo);
        editor.putBoolean(SettingsActivity.FULLSCREEN_MODE, fullscreenMode);

        editor.apply();
    }

    // Read settings from shared preferences
    private void restoreSettingsFromStorage() {
        SharedPreferences preferences = getPreferences(MODE_PRIVATE);

        resetWhiteTo = preferences.getLong(SettingsActivity.WHITE_MILLIS, 5 * 1000);
        resetBlackTo = preferences.getLong(SettingsActivity.BLACK_MILLIS, 5 * 1000);
        fullscreenMode = preferences.getBoolean(SettingsActivity.FULLSCREEN_MODE, false);
    }

    private boolean setKeepScreenOn(boolean keepOn) {
        if (keepOn) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            return true;
        }
        else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            return false;
        }
    }

    // Get seconds as an h:mm:ss formatted string.
    private String millisToString(long millis) {
        long seconds = (long)Math.ceil(millis / 1000.0);
        long h = seconds / (60 * 60);
        long m = seconds / 60 % 60;
        long s = seconds % 60;
        return String.format("%1$d:%2$02d:%3$02d", h, m, s);
    }

    // Adjust position and sizes for views.
    private void adjustLayout() {
        // Get metrics

        int orientation = getResources().getConfiguration().orientation;
        int longestSideLength = Math.max(fullscreenView.getHeight(), fullscreenView.getWidth());
        int shortestSideLength = Math.min(fullscreenView.getHeight(), fullscreenView.getWidth());

        // Set padding and update metrics
        int minPadding, multiButtonSidePadding, switchSidePadding;
        minPadding = (int) Math.hypot(longestSideLength, shortestSideLength) / 100;
        longestSideLength -= 2 * minPadding;

        if (((double) (shortestSideLength - minPadding * 2) / (double) longestSideLength) > 0.5) {
            // OK screen. Go for desired padding on short side and space buttons nicely:
            switchSidePadding = (2 * shortestSideLength - 4 * minPadding - longestSideLength) / 9;
            multiButtonSidePadding = 2 * switchSidePadding;
        }
        else if ((double) shortestSideLength / (double) longestSideLength < 0.5) {
            // Extremely narrow screen. Pad short sides extra to compensate:
            multiButtonSidePadding = switchSidePadding = 0;
            minPadding += (longestSideLength - 2 * shortestSideLength) / 2;
        }
        else {
            // Narrow screen. Reduce long side padding:
            int padding = shortestSideLength - longestSideLength / 2;
            switchSidePadding = padding * 1 / 3;
            multiButtonSidePadding = padding * 2 / 3;
        }

        if (orientation == Configuration.ORIENTATION_PORTRAIT)
            fullscreenView.setPaddingRelative(switchSidePadding, minPadding, multiButtonSidePadding, minPadding);
        else
            fullscreenView.setPaddingRelative(minPadding, multiButtonSidePadding, minPadding, switchSidePadding);

        // Adjust button sizes
        int switchDiameter = whiteButton.getHeight();

        if (longestSideLength > switchDiameter * 2) {
            // Switches too small. This can happen on large screens.
            switchDiameter = longestSideLength / 2;
            ViewGroup.LayoutParams whiteSwitchParams = whiteButton.getLayoutParams();
            ViewGroup.LayoutParams blackSwitchParams = blackButton.getLayoutParams();
            whiteSwitchParams.height = whiteSwitchParams.width = blackSwitchParams.height = blackSwitchParams.width = switchDiameter;
            whiteButton.setLayoutParams(whiteSwitchParams);
            blackButton.setLayoutParams(blackSwitchParams);
        }

        // MultiButton size
        ViewGroup.LayoutParams params = multiButton.getLayoutParams();
        int multiButtonDiameter = (int) (switchDiameter * (1d / 4d));
        params.height = params.width = multiButtonDiameter;
        multiButton.setLayoutParams(params);

        // Text size
        int textSize = switchDiameter / 7;
        whiteButtonText.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        blackButtonText.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
    }
}
