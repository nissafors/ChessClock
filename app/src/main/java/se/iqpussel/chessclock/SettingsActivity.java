package se.iqpussel.chessclock;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.Switch;


/**
 * Activity to let the user change the app settings
 *
 * @author Andreas Andersson
 */
public class SettingsActivity extends AppCompatActivity {

    public static final String WHITE_HOURS = "whiteSeconds";
    public static final String WHITE_MINUTES = "whiteMinutes";
    public static final String WHITE_SECONDS = "whiteSeconds";
    public static final String WHITE_MILLIS = "whiteMillis";
    public static final String BLACK_HOURS = "blackHours";
    public static final String BLACK_MINUTES = "blackMinutes";
    public static final String BLACK_SECONDS = "blackSeconds";
    public static final String BLACK_MILLIS = "blackMillis";
    public static final String FULLSCREEN_MODE = "fullscreenMode";


    // Views

    private NumberPicker whiteHoursNumberPicker;
    private NumberPicker whiteMinutesNumberPicker;
    private NumberPicker whiteSecondsNumberPicker;
    private NumberPicker blackHoursNumberPicker;
    private NumberPicker blackMinutesNumberPicker;
    private NumberPicker blackSecondsNumberPicker;
    private Switch fullscreenSwitch;
    private Button okButton;


    // AppCompatActivity

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        whiteHoursNumberPicker = (NumberPicker) findViewById(R.id.player_one_hours_picker);
        whiteHoursNumberPicker.setMinValue(0);
        whiteHoursNumberPicker.setMaxValue(48);
        whiteMinutesNumberPicker = (NumberPicker) findViewById(R.id.player_one_minutes_picker);
        whiteMinutesNumberPicker.setMinValue(0);
        whiteMinutesNumberPicker.setMaxValue(59);
        whiteSecondsNumberPicker = (NumberPicker) findViewById(R.id.player_one_seconds_picker);
        whiteSecondsNumberPicker.setMinValue(0);
        whiteSecondsNumberPicker.setMaxValue(59);

        blackHoursNumberPicker = (NumberPicker) findViewById(R.id.player_two_hours_picker);
        blackHoursNumberPicker.setMinValue(0);
        blackHoursNumberPicker.setMaxValue(48);
        blackMinutesNumberPicker = (NumberPicker) findViewById(R.id.player_two_minutes_picker);
        blackMinutesNumberPicker.setMinValue(0);
        blackMinutesNumberPicker.setMaxValue(59);
        blackSecondsNumberPicker = (NumberPicker) findViewById(R.id.player_two_seconds_picker);
        blackSecondsNumberPicker.setMinValue(0);
        blackSecondsNumberPicker.setMaxValue(59);

        fullscreenSwitch = (Switch) findViewById(R.id.fullscreen_switch);

        okButton = (Button) findViewById(R.id.settings_ok_button);
        okButton.setOnClickListener(okButtonListener);

        Intent intent = getIntent();
        long whiteMillis = intent.getLongExtra(WHITE_MILLIS, 5 * 60 * 1000);
        long blackMillis = intent.getLongExtra(BLACK_MILLIS, 5 * 60 * 1000);
        setNumberPickers(whiteMillis, blackMillis);
        fullscreenSwitch.setChecked(intent.getBooleanExtra(FULLSCREEN_MODE, false));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // Save number pickers
        outState.putInt(WHITE_HOURS, whiteHoursNumberPicker.getValue());
        outState.putInt(WHITE_MINUTES, whiteMinutesNumberPicker.getValue());
        outState.putInt(WHITE_SECONDS, whiteSecondsNumberPicker.getValue());
        outState.putInt(BLACK_HOURS, blackHoursNumberPicker.getValue());
        outState.putInt(BLACK_MINUTES, blackMinutesNumberPicker.getValue());
        outState.putInt(BLACK_SECONDS, blackSecondsNumberPicker.getValue());
        outState.putBoolean(FULLSCREEN_MODE, fullscreenSwitch.isChecked());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // Read back number pickers
        whiteHoursNumberPicker.setValue(savedInstanceState.getInt(WHITE_HOURS));
        whiteMinutesNumberPicker.setValue(savedInstanceState.getInt(WHITE_MINUTES));
        whiteSecondsNumberPicker.setValue(savedInstanceState.getInt(WHITE_SECONDS));
        blackHoursNumberPicker.setValue(savedInstanceState.getInt(BLACK_HOURS));
        blackMinutesNumberPicker.setValue(savedInstanceState.getInt(BLACK_MINUTES));
        blackSecondsNumberPicker.setValue(savedInstanceState.getInt(BLACK_SECONDS));
        fullscreenSwitch.setChecked(savedInstanceState.getBoolean(FULLSCREEN_MODE));
    }


    // Listeners

    View.OnClickListener okButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            // Send settings back to ChessClockActivity
            returnResults();
        }
    };


    // Methods

    // Return the resulting settings to ChessClockActivity
    private void returnResults() {
        Intent data = new Intent();
        long wh = whiteHoursNumberPicker.getValue();
        long wm = whiteMinutesNumberPicker.getValue();
        long ws = whiteSecondsNumberPicker.getValue();
        long bh = blackHoursNumberPicker.getValue();
        long bm = blackMinutesNumberPicker.getValue();
        long bs = blackSecondsNumberPicker.getValue();
        boolean fullScreenMode = fullscreenSwitch.isChecked();
        data.putExtra(WHITE_MILLIS, (wh * 60 * 60 + wm * 60 + ws) * 1000);
        data.putExtra(BLACK_MILLIS, (bh * 60 * 60 + bm * 60 + bs) * 1000);
        data.putExtra(FULLSCREEN_MODE, fullScreenMode);
        this.setResult(ChessClockActivity.SETTINGS_REQUEST_CODE, data);
        this.finish();
    }

    private void setNumberPickers(long whiteMillis, long blackMillis) {
        long whiteSeconds = whiteMillis / 1000;
        long blackSeconds = blackMillis / 1000;
        whiteHoursNumberPicker.setValue((int) whiteSeconds / (60 * 60));
        whiteMinutesNumberPicker.setValue((int) whiteSeconds / 60 % 60);
        whiteSecondsNumberPicker.setValue((int) whiteSeconds % 60);
        blackHoursNumberPicker.setValue((int) blackSeconds / (60 * 60));
        blackMinutesNumberPicker.setValue((int) blackSeconds / 60 % 60);
        blackSecondsNumberPicker.setValue((int) blackSeconds % 60);
    }
}
