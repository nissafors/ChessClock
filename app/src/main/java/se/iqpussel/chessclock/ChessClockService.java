package se.iqpussel.chessclock;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Binder;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.NotificationCompat;

/**
 * ChessClock As A Service!
 */
public class ChessClockService extends Service implements Pendulum.CallbackInterface {
    private static String TAG = "ChessClockService";

    // Enums

    /**
     * Possible states of a chess clock.
     */
    public enum State { WHITE_COUNTING, BLACK_COUNTING, PAUSED, OUT_OF_TIME }


    // Constants

    /**
     * Identifies this service when broadcasting.
     */
    public static final String CHESS_CLOCK_SERVICE_ID_STRING = "se.iqpussel.chessclock.chessClockService";

    private static final String ACTION_TOGGLE = "action_toggle";
    private static final String ACTION_PAUSE = "action_pause";
    private static final String ACTION_CONTINUE = "action_continue";
    private static final int NOTIFICATION_ID = 1337;


    // Members

    private State state = State.PAUSED;
    private State lastState = State.PAUSED;
    private Pendulum whitePendulum;
    private Pendulum blackPendulum;
    private long whiteTime;
    private long blackTime;
    private long whiteTimePassed;
    private long blackTimePassed;
    private long whiteLastStartedTS;
    private long blackLastStartedTS;
    private boolean isForeground = false;
    private boolean isActive = false;
    private NotificationCompat.Builder notificationBuilder;
    private NotificationManager notificationManager;


    // Service

    /**
     * Call getService() on an instance of ChessClockBinder to get a pointer to the
     * ChessClockService object.
     */
    public class ChessClockBinder extends Binder {
        // Return an instance of this ChessClockService.
        ChessClockService getService() {
            return ChessClockService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return new ChessClockBinder();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        whitePendulum = new Pendulum(this, 1000);
        blackPendulum = new Pendulum(this, 1000);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            // React on actions from notification

            String action = intent.getAction();

            if (action != null) {
                switch (action) {
                    case ACTION_TOGGLE:
                        if (state == State.WHITE_COUNTING)
                            setState(State.BLACK_COUNTING);
                        else
                            setState(State.WHITE_COUNTING);
                        break;
                    case ACTION_PAUSE:
                        setState(State.PAUSED);
                        break;
                    case ACTION_CONTINUE:
                        setState(lastState);
                        break;
                }
            }
        }

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // Probably not needed, but make sure all threads are terminated anyway.
        whitePendulum.stop();
        blackPendulum.stop();
    }


    // Pendulum.CallbackInterface

    @Override
    public void onTick() {
        broadcastTimes();
        checkOutOfTime();
    }


    // Public methods

    /**
     * Start or paus the clock. Does nothing if the current newState is out of time or if trying to
     * set the same newState twice.
     *
     * @param newState .WHITE_COUNTING, .BLACK_COUNTING or PAUSED
     */
    public void setState(State newState) {
        if (this.state == State.OUT_OF_TIME || this.state == newState) {
            return;
        }

        long nowTS = SystemClock.elapsedRealtime();

        switch (newState) {

            case PAUSED:
                stopWhitePendulum(nowTS);
                stopBlackPendulum(nowTS);
                break;

            case WHITE_COUNTING:
                stopBlackPendulum(nowTS);
                whiteLastStartedTS = nowTS;
                whitePendulum.start(whiteTimePassed % 1000);
                break;

            case BLACK_COUNTING:
                stopWhitePendulum(nowTS);
                blackLastStartedTS = nowTS;
                blackPendulum.start(blackTimePassed % 1000);
                break;
        }

        isActive = true;
        this.lastState = this.state;
        this.state = newState;
    }

    /**
     * Send this service to the foreground.
     */
    public void toForeground() {
        Intent notificationIntent = new Intent(this, ChessClockActivity.class);
        notificationIntent.setAction(CHESS_CLOCK_SERVICE_ID_STRING);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        setNotificationBuilder(pendingIntent);

        startService(new Intent(this, ChessClockService.class));
        startForeground(NOTIFICATION_ID, notificationBuilder.build());
        isForeground = true;
    }

    /**
     * Return service to background.
     */
    public void toBackground() {
        stopForeground(true);
        isForeground = false;
    }

    /**
     * Return the current chessclock state
     *
     * @return state as a ChessClockService.State enum
     */
    public State getState() {
        return state;
    }

    /**
     * Service becomes active when setState() is first called and resets to inactive on setClocks().
     *
     * @return
     */
    public boolean isActive() {
        return isActive;
    }

    /**
     * Get whites time left.
     *
     * @return time left in milliseconds
     */
    public long getWhiteMillisLeft() {
        long timeLeft;

        if (state == State.WHITE_COUNTING)
            timeLeft = whiteTime - (whiteTimePassed + SystemClock.elapsedRealtime() - whiteLastStartedTS);
        else
            timeLeft = whiteTime - whiteTimePassed;

        return timeLeft < 0 ? 0 : timeLeft;
    }

    /**
     * Get blacks time left.
     *
     * @return time left in milliseconds
     */
    public long getBlackMillisLeft() {
        long timeLeft;

        if (state == State.BLACK_COUNTING)
            timeLeft = blackTime - (blackTimePassed + SystemClock.elapsedRealtime() - blackLastStartedTS);
        else
            timeLeft = blackTime - blackTimePassed;

        return timeLeft < 0 ? 0 : timeLeft;
    }

    /**
     * Set clocks for white and black. State will be set to PAUSED or OUT_OF_TIME.
     *
     * @param whiteTime     whites clock setting in milliseconds
     * @param blackTime     blacks clock setting in milliseconds
     */
    public void setClocks(long whiteTime, long blackTime) {
        this.setClocks(whiteTime, blackTime, whiteTime, blackTime);
    }

    /**
     * Set clocks and time left for white and black. Use to restore instance state.
     *
     * @param whiteTime     whites clock setting in milliseconds
     * @param blackTime     blacks clock setting in milliseconds
     */
    public void setClocks(long whiteTime, long blackTime, long whiteTimeLeft, long blackTimeLeft) {
        // Error checking
        if (state == State.WHITE_COUNTING || state == State.BLACK_COUNTING) {
            return;
        }

        this.whiteTime = whiteTime;
        this.blackTime = blackTime;
        this.whiteTimePassed = whiteTime - whiteTimeLeft;
        this.blackTimePassed = blackTime - blackTimeLeft;

        state = State.PAUSED;
        isActive = false;
        checkOutOfTime();
    }


    // Private methods

    // Populate notification related members
    private void setNotificationBuilder(PendingIntent pendingIntent) {
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationBuilder = new NotificationCompat.Builder(this);
        notificationBuilder.setSmallIcon(R.mipmap.notification);
        notificationBuilder.setLargeIcon(Bitmap.createBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher)));
        notificationBuilder.setContentTitle(getResources().getString(R.string.notify_content_title));
        notificationBuilder.setContentIntent(pendingIntent);
        notificationBuilder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
        notificationBuilder.addAction(getAction(ACTION_TOGGLE));
        notificationBuilder.addAction(getAction(ACTION_PAUSE));
    }

    // Return a notification action. Possible action values are ACTION_TOGGLE, ACTION_PAUSE
    // and ACTION_CONTINUE.
    private NotificationCompat.Action getAction(String action) {
        int icon;
        String text;

        switch (action) {
            case ACTION_TOGGLE:
                if (state == State.WHITE_COUNTING)
                    icon = R.mipmap.control_toggle_white;
                else
                    icon = R.mipmap.control_toggle_black;
                text = getResources().getString(R.string.action_toggle);
                break;
            case ACTION_PAUSE:
                icon = R.mipmap.control_pause;
                text = getResources().getString(R.string.action_pause);
                break;
            case ACTION_CONTINUE:
                icon = R.mipmap.control_play;
                text = getResources().getString(R.string.action_continue);
                break;
            default:
                return null;
        }

        Intent intent = new Intent(getApplicationContext(), ChessClockService.class);
        intent.setAction(action);
        PendingIntent pendingIntent = PendingIntent.getService(getApplicationContext(), 1, intent, 0);
        return new NotificationCompat.Action.Builder(icon, text, pendingIntent).build();
    }

    // Update the notification depending on state and set text.
    private void updateNotification(String text) {
        if (notificationBuilder != null && notificationManager != null) {
            notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(text));
            notificationBuilder.setContentText(text);

            notificationBuilder.mActions.clear();
            if (state == State.WHITE_COUNTING || state == State.BLACK_COUNTING) {
                notificationBuilder.addAction(getAction(ACTION_TOGGLE));
                notificationBuilder.addAction(getAction(ACTION_PAUSE));
            }
            else if (state == State.PAUSED){
                notificationBuilder.addAction(getAction(ACTION_CONTINUE));
            }

            notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());
        }
    }

    // Stop the white pendulum and conditionally update time
    private void stopWhitePendulum(long nowTS) {
        whitePendulum.stop();
        if (this.state == State.WHITE_COUNTING)
            whiteTimePassed += nowTS - whiteLastStartedTS;
    }

    // Stop the black pendulum and conditionally update time
    private void stopBlackPendulum(long nowTS) {
        blackPendulum.stop();
        if (this.state == State.BLACK_COUNTING)
            blackTimePassed += nowTS - blackLastStartedTS;
    }

    // Broadcast time left for white and black, either to the main activity or the notification.
    private void broadcastTimes() {
        if (isForeground) {
            String text = millisToString(getWhiteMillisLeft()) + "\n" + millisToString(getBlackMillisLeft());
            updateNotification(text);
        }
        else {
            Intent intent = new Intent(CHESS_CLOCK_SERVICE_ID_STRING);
            intent.putExtra("white", getWhiteMillisLeft());
            intent.putExtra("black", getBlackMillisLeft());
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        }
    }

    // Check if white or black is out of time and, if so, stop pendulums and set state.
    private void checkOutOfTime() {
        if (getWhiteMillisLeft() <= 0 || getBlackMillisLeft() <= 0) {
            long nowTS = SystemClock.elapsedRealtime();
            stopWhitePendulum(nowTS);
            stopBlackPendulum(nowTS);
            state = State.OUT_OF_TIME;
        }
    }

    // Get seconds as an h:mm:ss formatted string.
    private String millisToString(long millis) {
        long seconds = (long)Math.ceil(millis / 1000.0);
        long h = seconds / (60 * 60);
        long m = seconds / 60 % 60;
        long s = seconds % 60;
        return String.format("%1$d:%2$02d:%3$02d", h, m, s);
    }
}
