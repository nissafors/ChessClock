package se.iqpussel.chessclock;

import android.os.SystemClock;
import android.util.Log;

import static java.lang.Thread.sleep;

/**
 * Created by andreas on 2016-12-15.
 *
 * A class providing callbacks on given interval.
 */
public class Pendulum {
    // Constants

    /**
     * Resolution in milliseconds. Smaller values are heavier on the CPU.
     */
    public static long RESOLUTION = 10;


    // Members

    private Thread thread;
    private long tickInterval;
    private long startAt;
    private CallbackInterface callback;


    // Constructor

    /**
     * Construct a new pendulum.
     *
     * @param callback a class implementing Pendulum.CallbackInterface
     * @param tickInterval the interval in milliseconds between ticks. Must be larger than SLEEP_TIME.
     */
    public Pendulum(CallbackInterface callback, long tickInterval) {
        if (tickInterval <= RESOLUTION)
            throw new IllegalArgumentException("tickInterval too short.");

        this.callback = callback;
        this.tickInterval = tickInterval;
    }


    // Public methods

    /**
     * Start the pendulum.
     */
    public void start() {
        this.start(0);
    }

    /**
     * Start the pendulum a bit into the first tick.
     *
     * @param startAt milliseconds into first tick
     */
    public void start(long startAt) {
        if (thread == null) {
            this.startAt = startAt;
            thread = new Thread(runnable);
            thread.start();
        }
    }

    /**
     * Stop the pendulum.
     */
    public void stop() {
        if (thread != null) {
            thread.interrupt();
            thread = null;
        }
    }


    // Interfaces

    /**
     * Implement this interface to receive callbacks when tick events occur.
     */
    public interface CallbackInterface {
        void onTick();
    }


    // Private stuff

    // Block generating tick events. To run in its own thread.
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            boolean running = true;
            long timeToFirstTick = tickInterval - startAt;
            long waitUntil = SystemClock.elapsedRealtime() + timeToFirstTick;

            while (running) {
                while (SystemClock.elapsedRealtime() < waitUntil) {
                    if (Thread.interrupted()) {
                        running = false;
                        break;
                    }

                    // We really don't need better resolution than this and some
                    // sleeping saves enormous amounts of CPU power.
                    try {
                        sleep(RESOLUTION);
                    } catch (InterruptedException e) {
                        running = false;
                        break;
                    }
                }

                waitUntil += tickInterval;
                callback.onTick();
            }
        }
    };
}
